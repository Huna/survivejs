const webpack = require('webpack');
const path = require('path');
const globby    = require('globby');
const { mode } = require('webpack-nano/argv');
const { merge } = require('webpack-merge');
const parts = require('./webpack.parts');
const pkg = require('./package.json');


const publicPath = 'public/';
const PATHS = {
	assets: path.join(__dirname, `themes/mytheme/assets`),
	build: path.join(__dirname, 'public/build'),
};

const comonCfg = merge([
	parts.CleanWebpack(),
	{
		entry: {
			main: [`${PATHS.assets}/js/main.js`]
				.concat(globby.sync(
					['themes/mytheme/assets/img',],
					{expandDirectories: {extensions: ['png', 'jpg', 'gif'],},},
				),),
		},
		output: {
			path: PATHS.build,
			publicPath,
		},
		bail: true,
		profile: true,
		recordsPath: path.join(__dirname, 'records.json'),
		resolve: {
			alias: {
			},
			modules: [
				path.resolve(__dirname),
				'node_modules',
			],
		},
	}
]);
const prodCfg   = merge([
	{
		output: {
			chunkFilename: 'js/[name].[chunkhash].js',
			filename: 'js/[name].[chunkhash].js',
			pathinfo: false,
		},
	},
	parts.loadJavaScript({
		name: 'js/[name].[hash].[ext]',
		include: `${PATHS.assets}/js`,
		exclude: [
			'/node_modules/',
		],
	}),
	parts.loadImages({
		sizeLimit: 1,
		filename: 'img/[name].[hash][ext]',
	}),
	parts.splitChunks(),

	parts.assetsManifest({output: 'manifest.json',}),

]);

const devCfg   = merge([
	{
		output: {
			chunkFilename: 'js/[name].js',
			filename: 'js/[name].js',
			pathinfo: false,
		},
	},
	parts.loadJavaScript({
		name: 'js/[name].[ext]',
		include: `${PATHS.assets}/js`,
		exclude: [
			'/node_modules/',
		],
	}),
	parts.loadImages({
		sizeLimit: 1,
		filename: 'img/[name][ext]',
	}),
	parts.splitChunks(),

	parts.SourceMapDevTool({
		filename: '[file].map[query]',
		exclude: [
		],
	}),

	parts.assetsManifest({output: 'manifest.json',}),


]);

const getConfig = (mode) => {
	process.env.NODE_ENV = mode;
	switch (mode) {
		case 'production':
			return merge(comonCfg, prodCfg, { mode });
		case 'development':
			return merge(comonCfg, devCfg, { mode });
		default:
			throw new Error(`Trying to use an unknown mode, ${mode}`);
	}
};
module.exports = getConfig(mode);

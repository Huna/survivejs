const webpack = require('webpack');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');

const pkg = require('./package.json');

exports.CleanWebpack = (path) => ({
	plugins: [
		new CleanWebpackPlugin({
				dry: false,
				verbose: true,
				cleanStaleWebpackAssets: true,
				protectWebpackAssets: false,
				cleanOnceBeforeBuildPatterns: ['**/*'],
		}),
	],
});

exports.loadJavaScript = ({ include, exclude, name } = {}) => ({
	module: {
		rules: [
			{
				test: /\.js$/,
				sideEffects: true,
				include,
				exclude,
				use: [
					{
						loader: 'babel-loader',
						options: {
							babelrc: true,
							sourceMap: true,
						},
					},
				],
			},
		],
	},
});

exports.splitChunks = () => ({
	optimization: {
		runtimeChunk: { name: 'runtime_manifest', },
		splitChunks: {
			cacheGroups: {
				commons: {
					test: /(node_modules\/(.*)\.js)/,
					name: 'vendor',
					chunks: 'all',
					priority: -10,
				},
			},
		},
	},
});

exports.loadImages = ({ sizeLimit, filename } = {}) => ({
	module: {
		rules: [
			{
				test: /\.(gif|png|jpe?g|svg|ico)$/,
				type: 'asset',
				generator: { filename, },
				parser: { dataUrlCondition: { maxSize: sizeLimit },},
			},
		],
	},
});

exports.SourceMapDevTool = ({ filename, exclude }) => ({
	plugins: [
		new webpack.SourceMapDevToolPlugin({
			filename,
			exclude,
			moduleFilenameTemplate: undefined,
			fallbackModuleFilenameTemplate: undefined,
			append: null,
			module: true,
			columns: true,
			noSources: false,
			namespace: '',
		}),
	],
});

exports.assetsManifest = ({ output }) => ({
	plugins: [
		new WebpackAssetsManifest({
			enabled: true,
			contextRelativeKeys: false,
			entrypoints: false,
			entrypointsKey: false,
			entrypointsUseAssets: false,
			integrity: true,
			integrityHashes: ['sha256', 'sha384', 'sha512'],
			merge: false,
			output,
			publicPath(filename, manifest) {
				switch (manifest.getExtension(filename).substr(1).toLowerCase()) {
					case 'jpg': case 'jpeg': case 'gif': case 'png': case 'svg': case 'ico':
						return `/build/${filename}`;
					case 'css':
						return `/build/${filename}`;
					case 'js':
						return `/build/${filename}`;
					default:
						return `/build/${filename}`;
				}
			},
			sortManifest: true,
/*
			transform: (manifest) => {
				const ret = {};
				Object.entries(manifest).forEach(([k, v]) => {
					if (k.startsWith('img/')) {
						ret[k.split('img/')[1]] = v;
					} else {
						ret[k] = v;
					}
				});
				return ret;
			},
*/
			customize: (o) => {
				if (o.key.startsWith('img/')) {
					return { key: o.key.split('img/')[1], value: o.value };
				}

				return o;
			},
		}),
	],
});


